<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\group\Entity\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContactForm.
 *
 * @package Drupal\clubsy_general\Form
 */
class EditGroupForm extends ConfigFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $id;


  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'bookclub.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_book_club_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    $group = Group::load($this->id);
    $group = Group::load($id);
    $uid = $group->getOwnerId();
    $account = $current_user = \Drupal::currentUser()->id();
    if ($uid == $account || $account == 1) {
      $form['id'] = $this->id;
      $form['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#required' => TRUE,
        '#default_value' => $group->label(),
      ];

      $form['wallpaper'] = [
        '#type' => 'managed_file',
        '#title' => t('Upload new Wallpaper'),
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg'],
          'file_validate_size' => [25600000],
        ],
        '#theme' => 'image_widget',
        '#preview_image_style' => 'medium',
        '#upload_location' => 'public://group-wallpaper',
        '#required' => FALSE,
        '#default_value' => NULL,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#title' => $this->t('Create new Group'),
        '#required' => TRUE,
        '#value' => t('Edit Book Club.'),
      ];


      return $form;
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group = Group::load($this->id);
    $group->set('label', $form_state->getValue('title'));
    if ($form_state->getValue('wallpaper') != NULL) {
      $group->set('field_wallpaper', $form_state->getValue('wallpaper'));
    }
    $group->save();
    $this->messenger->addMessage('Succesfully edited group.', 'status');
  }

}
