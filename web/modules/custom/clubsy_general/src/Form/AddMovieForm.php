<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\system\Plugin\migrate\process\d6\TimeZone;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AddMovieForm.
 *
 * @package Drupal\clubsy_general\Form
 */
class AddMovieForm extends ConfigFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'book.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_movie_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    $form['id'] = $id;
    $group = Group::load($id);
    $uid = $group->getOwnerId();
    $account = $current_user = \Drupal::currentUser()->id();
    if ($uid == $account || $account == 1) {
      $form['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#required' => TRUE,
      ];

      $form['director'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Director'),
        '#description' => $this->t('The director of the movie.'),
        '#required' => TRUE,
      );

      $form['year'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Year'),
        '#description' => $this->t('The release date of this book.'),
        '#required' => TRUE,
      );

      $form['summary'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Short Summary'),
        '#description' => $this->t('Give a short summary of the movie.'),
        '#required' => TRUE,
      );

      $form['cover'] = [
        '#type' => 'managed_file',
        '#title' => t('Movie Cover '),
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg'],
          'file_validate_size' => [25600000],
        ],
        '#theme' => 'image_widget',
        '#preview_image_style' => 'medium',
        '#upload_location' => 'public://movie-cover',
        '#required' => TRUE,
      ];

      $form['deadline'] = array(
        '#type' => 'date',
        '#title' => $this->t('Deadline'),
        '#description' => $this->t('Book Deadline'),
        '#required' => TRUE,
      );

      $form['submit'] = [
        '#type' => 'submit',
        '#title' => $this->t('Create new Group'),
        '#required' => TRUE,
        '#value' => t('Add this movie'),
      ];


      return $form;
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $groupEntity = Group::load($form['id']);
    $node = Node::create([
      'type' => 'movie',
      'title' => $form_state->getValue('title'),
    ]);
    $node->save();
    $groupEntity->addContent($node,'group_node:movie', [
      'field_cover' => $form_state->getValue('cover'),
      'field_deadline' => $form_state->getValue('deadline'),
      'field_summary' => $form_state->getValue('summary'),
      'field_director' => $form_state->getValue('director'),
      'field_year' => $form_state->getValue('year'),
      'field_rating' => 0,
      'field_reviews' => 2
    ]);
    $groupEntity->save();

    $this->messenger->addMessage('Succesfully added new movie to the club.', 'status');
    $this->redirect('entity.group.canonical', ['group' => $form['id']])->send();

  }

}
