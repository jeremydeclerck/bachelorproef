<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class AddMemberForm extends ConfirmFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $id;

  /**
   * ID of the Group Node Content to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL, $uid = NULL) {
    $this->id = $id;
    $this->uid = $uid;
      $group = Group::load($id);
      $uid = $group->getOwnerId();
      $account = $current_user = \Drupal::currentUser()->id();
      if ($uid == $account || $account == 1) {
        return parent::buildForm($form, $form_state);
      }
      else {
          throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
        }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = User::load($this->uid);

    $group = Group::load($this->id);
    $gid = $group->getOwnerId();
    $route_params = ['group' => $this->id];

    if (!$group->getMember($user)) {
      $group->addMember($user, [
      'group_roles' => array('book_club-invited', 'movie_club-invited'),

      ]);
      $group->save();
      $this->messenger->addMessage('Successfully sent a member request.', 'success');
      $form_state->setRedirect('entity.group.canonical', $route_params);
    } else {
      $form_state->setRedirect('entity.group.canonical', $route_params);
      $this->messenger->addMessage('This user is already a member of your club or has an invite pending.', 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "add_member_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group.canonical', ['group' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Send Member request');
  }

}