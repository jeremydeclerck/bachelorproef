<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class DeleteGroupForm extends ConfirmFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    $group = Group::load($id);
    $uid = $group->getOwnerId();
    $account = $current_user = \Drupal::currentUser()->id();
    if ($uid == $account || $account == 1) {
      return parent::buildForm($form, $form_state);
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group = Group::load($this->id);
    $group->delete();
    $this->messenger->addMessage('Succesfully deleted group.', 'status');
    $this->redirect('<front>')->send();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "delete_group_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group.canonical', ['group' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Remove Group.');
  }

}