<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class DeleteBookForm extends ConfirmFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $id;

  /**
   * ID of the Group Node Content to delete.
   *
   * @var int
   */
  protected $gnid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL, $gnid = NULL) {
    $this->id = $id;
    $this->gnid = $gnid;
    $group = Group::load($id);
    $uid = $group->getOwnerId();
    $account = $current_user = \Drupal::currentUser()->id();
    if ($uid == $account || $account == 1) {
      return parent::buildForm($form, $form_state);
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gc = GroupContent::load($this->gnid);
    $gcid = $gc->getOwnerId();

    $group = Group::load($this->id);
    $gid = $group->getOwnerId();
    $route_params = ['group' => $this->id];

    $gc->delete();
    $form_state->setRedirect('entity.group.canonical', $route_params);
    $this->messenger->addMessage($gc->label() . '  was deleted');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "delete_book_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group_content.canonical', [
      'group' => $this->id,
      'group_content' => $this->gnid,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('This cannot be undone.');
  }

}