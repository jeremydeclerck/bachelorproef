<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class JoinGroupForm extends ConfirmFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * ID of the Group Node Content to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $uid = NULL, $id = NULL) {
    $this->uid = $uid;
    $this->id = $id;
    $current_user = \Drupal::currentUser()->id();
    if ($this->uid == $current_user) {
      return parent::buildForm($form, $form_state);
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = User::load($this->uid);
    $group = Group::load($this->id);
    $group->removeMember($user);
    $group->save();
    $group->addMember($user, [
      'group_roles' => array('book_club-club_member', 'movie_club-club_member')
    ]);
    $group->save();
    $this->messenger->addMessage("Congratulations, you joined a group!", 'status');
    $this->redirect('entity.group.canonical', ['group' => $this->id])->send();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "join_group_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.canonical', ['user' => $this->uid]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('This cannot be undone.');
  }

}