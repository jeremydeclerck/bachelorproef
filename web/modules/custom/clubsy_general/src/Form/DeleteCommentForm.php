<?php

namespace Drupal\clubsy_general\Form;

use Drupal\comment\Entity\Comment;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class DeleteCommentForm extends ConfirmFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * ID of Group.
   *
   * @var int
   */
  protected $gid;

  /**
   * ID of the Group Node Content to delete.
   *
   * @var int
   */
  protected $cid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $gid = NULL, $cid = NULL) {
    $this->gid = $gid;
    $this->cid = $cid;
    $group = Group::load($gid);
    $goid = $group->getOwnerId();
    $account = $current_user = \Drupal::currentUser()->id();
    if ($goid == $account || $account == 1) {
      return parent::buildForm($form, $form_state);
    }
    else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $comment = Comment::load($this->cid);
    $comment->delete();

    $route_params = ['group' => $this->gid];
    $form_state->setRedirect('entity.group.canonical', $route_params);
    $this->messenger->addMessage('Successfully deleted comment.');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "delete_comment_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.group.canonical', [
      'group' => $this->gid,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('This cannot be undone.');
  }

}