<?php

namespace Drupal\clubsy_general\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContactForm.
 *
 * @package Drupal\clubsy_general\Form
 */
class AddGroupForm extends ConfigFormBase {

  /**
   * Defines MessengerInterface variable.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'bookclub.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'book_club_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
    ];

    $form['wallpaper'] = [
      '#type' => 'managed_file',
      '#title' => t('Book Club Wallpaper'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
      '#upload_location' => 'public://group-wallpaper',
      '#required' => TRUE,
    ];

    $options = array(
      'book_club' => t('Book Club'),
      'movie_club' => t('Movie Club'),
    );

    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('What kind of club?'),
      '#description' => t('Choose your club.'),
      '#options' => $options,
    );

    $form['submit'] = [
      '#type' => 'submit',
      '#title' => $this->t('Create new Group'),
      '#required' => TRUE,
      '#value' => t('Create new Club'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $group = Group::create([
      'type' => $form_state->getValue('type'),
      'label' => $form_state->getValue('title'),
      'field_wallpaper' => $form_state->getValue('wallpaper'),
    ]);
    $group->save();

    $this->messenger->addMessage('Succesfully Created new group.', 'status');
    $this->redirect('entity.group.canonical', ['group' => $group->id()])->send();


  }

}
