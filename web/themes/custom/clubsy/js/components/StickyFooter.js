// ---------------------------------------
// :: STICKY FOOTER
// ---------------------------------------

var StickyFooter = (function () {

	// CONSTRUCTOR
	// ---------------------------------------
    var StickyFooter = function (el) {
		this.el = el;

		this.init();
		this.initEvents();
    };

	// INITIALISE EVENTS
	// ---------------------------------------
	StickyFooter.prototype.initEvents = function() {
		window.addEventListener("resize", this.handleResize.bind(this));
	};

	// GENERAL INITIALISE
	// ---------------------------------------
    StickyFooter.prototype.init = function() {
		this.handleResize();
    };

	// HELPERS
	// ---------------------------------------
	StickyFooter.prototype.setHeight = function() {
		this.height = this.el.offsetHeight;
	};

	// HANDLERS
	// ---------------------------------------
	StickyFooter.prototype.handleResize = function() {
		const bodyHeight = document.body.offsetHeight;

		this.setHeight();

		if (bodyHeight > window.innerHeight) {
			//document.body.classList.remove("has-sticky-footer");
			//document.body.style.paddingBottom = "0px";
			document.querySelector(".o-footer").classList.remove("stickybottom");
		} else {
			//document.body.classList.add("has-sticky-footer");
			//document.body.style.paddingBottom = this.height + "px";
			document.querySelector(".o-footer").classList.add("stickybottom");
		}

		//
	};

    return StickyFooter;
})();
