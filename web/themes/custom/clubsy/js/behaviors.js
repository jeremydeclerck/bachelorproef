(function ($, Drupal) {
    /**
     * Keeps the footer in place at the bottom of the page
     * in case the screen height is bigger than the body height.
     *
     * @type {{attach: Drupal.behaviors.StickyFooter.attach}}
     */
    Drupal.behaviors.StickyFooter = {
        attach: function attach(context) {
            var footer = document.querySelector(".o-footer");
            var stickyFooter = new StickyFooter(footer);
        }
    };

    /**
     * Provides mobile navigation menu functionality.
     *
     * @type {{attach: Drupal.behaviors.Menu.attach}}
     */
    Drupal.behaviors.Menu = {
        attach: function attach(context) {
            var headerSelector = document.querySelector(".js-menu-toggle");
            headerSelector.addEventListener('click', function(){
                document.querySelector(".js-menu-flyout").classList.toggle('is-open');
                document.body.classList.toggle('u-no-scroll');
            });
        }
    };
})(jQuery, Drupal);